package main

import (
	"fmt"
)

func main() {
	c := make(chan string)
	go PrintName(c)
	for msg := range c {
		fmt.Println(msg)
	}
}
func PrintName(c chan string) {
	for i := 0; i < 5; i++ {
		c <- "diksha"
	}
	close(c)
}
