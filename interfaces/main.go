package main

import "fmt"

type print interface {
	void() string
}

type Name struct {
	name string
}

func (c Name) void() string {
	return c.name
}

func main() {
	d := Name{"Diksha"}
	//    call through interface
	fmt.Println(print.void(d))
	//    call through method
	fmt.Println(d.void())
}
